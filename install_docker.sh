#!/bin/bash
apt-get remove docker docker-engine docker-ce docker.io -y

apt-get update -y

apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual

apt-get update -y

apt-get install -y apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg |sudo apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update -y

apt-get install docker-ce -y

docker version

curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

docker-compose --version

